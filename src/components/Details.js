import React from 'react';

const details = ({ starship }) => {
    let movies = starship.films
    return(

        <div className="generalBox">
            <div className="transparent-box">
                <h2 className="title">{starship.name}</h2>
                <p>{starship.model}</p>
                <hr/>

                <h4 className="title">Fabricante</h4>
                <p>{starship.manufacturer}</p>
                
                <h4 className="title">Largo</h4>
                <p>{starship.length}</p>
               
                <h4 className="title">Valor</h4>
                <p>{starship.cost_in_credits}</p>
                
                <h4 className="title">Cantidad pasajeros</h4>
                <p>{starship.passengers}</p>
                
            </div>
            {
                movies &&
                <div className="transparent-box last">
                    <h2 className="title">Películas</h2>
                    <hr/>
                    {
                        movies.map( (e, key) => <p key={key}><a href={e} target="_blank">{e}</a></p>)
                    }
                </div>
            }
            
        </div>
    )
}

export default details
