import axios from "axios"

export const FETCH_STARSHIPS_REQUEST = 'FETCH_STARSHIPS_REQUEST';
export const FETCH_STARSHIPS_SUCCESS = 'FETCH_STARSHIPS_SUCCESS';
export const FETCH_STARSHIPS_ERROR = 'FETCH_STARSHIPS_ERROR';

export const fetchStarshipsRequest = () => {
    return {
        type: FETCH_STARSHIPS_REQUEST
    }
}

export const fetchStarshipsSuccess = (starships) => {
    return {
        type: FETCH_STARSHIPS_SUCCESS,
        payload: starships,
    }
}

export const fetchStarshipsError = (error) => {
    return {
        type: FETCH_STARSHIPS_ERROR,
        payload: error,
    }
}

const fetchStarships = () => {
    return (dispatch) => {
        dispatch(fetchStarshipsRequest());
        axios.get(`https://swapi.dev/api/starships/`)
            .then( response => {
                dispatch(fetchStarshipsSuccess(response.data.results))
            }).catch(e => {
                if(e.message.includes('404')){
                    dispatch(fetchStarshipsError('No se encontró ninguna nave'))
                }
                dispatch(fetchStarshipsError(e.message))
            })
    }
}

export default fetchStarships