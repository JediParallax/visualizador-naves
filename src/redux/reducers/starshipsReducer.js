import { FETCH_STARSHIPS_REQUEST, FETCH_STARSHIPS_SUCCESS, FETCH_STARSHIPS_ERROR } from "../actions/getStarships";

const initial_state = {
    loading: false,
    starships: null,
    error: null
}

const starshipsList = (state = initial_state, action) => {
    switch(action.type){

        case FETCH_STARSHIPS_REQUEST:
            return{
                ...state,
                loading: true
            }
        case FETCH_STARSHIPS_SUCCESS:
            return{
                ...state,
                loading: false,
                starships: action.payload,
                error: null
            }
        case FETCH_STARSHIPS_ERROR:
            return{
                ...state,
                loading: false,
                starships: null,
                error: action.payload
            }
        default: return state
    }
}

export default starshipsList