import { combineReducers } from "redux";
import starshipsList from "./starshipsReducer";

const rootReducer = combineReducers({
    starshipsList
})

export default rootReducer
