import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line
import style from './style.css'
import App from './App';
import { Provider} from 'react-redux'
import store from './redux/store'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

