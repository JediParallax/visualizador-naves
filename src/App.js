import React,{useEffect,useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import fetchStarships from './redux/actions/getStarships'
import Details from './components/Details'

function App() {
  
  const [starship, setStarship] = useState({}) 
  const dispatch = useDispatch()
  const { starships, error, loading } = useSelector( (state) => state.starshipsList) 
  useEffect( () => {
    dispatch(fetchStarships())
  }, [])

  const onChangeSelect = (e) => {
    const selectedStarshipName = e.target.value;
    const starshipDetails = starships.filter( e => e.name === selectedStarshipName)[0]
    setStarship(starshipDetails)
  }

  return (
    <div className="container">
      <select 
        className="form-select mt-4"
        onChange={ (e) => {onChangeSelect(e)}} >
        <option>{`${loading ? 'loading' : 'Seleccione una nave...'}`}</option>
        { 
        starships &&
        starships.map( (e, key) => <option key={key} value={e.name}>{e.name}</option>) 
        }
        </select>
        {
          error &&
          <div className="alert alert-secondary mt-4 mb-4">
            {error}
          </div>
        }
        {
          starship && Object.keys(starship).length > 0 &&
          <Details starship={starship}/>
        }
    </div>
  );
}

export default App;
